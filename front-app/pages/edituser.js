import Head from "next/head";
import { useContext, useEffect, useState } from "react";
import Layout from "../components/layout";
import UserEditLayout from "../components/userEditLayout";
import config from "../config";
import UserContext from "../store/user_context";

export default function EditUser(props) {

    const userCtx = useContext(UserContext);

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (!localUser) {
            window.location.href = '/login'
        } else {
            const currentTime = new Date().getTime();
            const tokenExpiry = new Date(localUser.refreshTokenExpiry).getTime();
            const timeDifference = tokenExpiry - currentTime;
            if (timeDifference < 0) {
                window.localStorage.removeItem('user')
                window.location.href = '/login'
            } else {
                if (localUser.r_name != 'Admin') {
                    window.location.href = '/'
                } else {
                    setIsLoading(false);
                }
            }
        }
    });

    if (isLoading) {
        return (
            <div>Loading...</div>
        )
    }

    return (
        <Layout>
            <Head>
                <title>Edit Users</title>
            </Head>
            <UserEditLayout context={userCtx} roles={props.roles} />
        </Layout>
    )
}

export async function getStaticProps(context) {
    const res = await fetch(`${config.app.baseUrl}/roles`, {
        method: 'GET',
        body: JSON.stringify(),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if (res.status == '200') {
        const data = await res.json();
        return {
            props: {
                roles: data
            }
        }
    }
    return {
        notFound: true,
    }
}