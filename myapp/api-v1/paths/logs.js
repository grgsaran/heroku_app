const config = require("../../config");
const logService = require("../services/logService");
const authValidator = require("../validators/auth_validator");

module.exports = {
  parameters: [
    {
      name: 'api_key',
      in: 'header',
      type: 'string',
      description: 'token',
    },
  ],
  get: GET,
  post: POST,
}

async function GET(req, res, next) {
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    const logs = await logService.getLogs();
    if (Object.keys(logs).length != 0) {
      res.status(config.status_code.ok).send(logs);
    } else {
      res.status(config.status_code.no_content).send();
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

async function POST(req, res, next) {
  console.log(`${req.body.login_time} ${req.body.u_name}`);
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    if (req.body.login_time.length > 0 && req.body.u_name.length > 0) {
      const query = await logService.addLog(req.body);
      console.log(query);
      if (query > 0) {
        res.status(config.status_code.created).send("New log created");
      } else {
        res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Cannot create log" });
      }

    } else {
      res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Invalid data" });
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

GET.apiDoc = {
  summary: "Fetch all logs.",
  operationId: "getLogs",
  tags: ['Logs'],
  responses: {
    200: {
      description: "Logs.",
      schema: {
        type: "object",
        items: {
          $ref: "#/definitions/Log",
        },
      },
    },
    204: {
      description: "Logs not found",
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
}

POST.apiDoc = {
  summary: "Create new log.",
  operationId: "addLog",
  tags: ['Logs'],
  consumes: ["application/json"],
  parameters: [
    {
      in: "body",
      name: "log",
      schema: {
        $ref: "#/definitions/Log",
      },
    },
  ],
  responses: {
    201: {
      description: "Created",
    },
    400: {
      description: 'Something happened...',
      schema: {
        $ref: '#/definitions/ErrorResponse'
      }
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
}