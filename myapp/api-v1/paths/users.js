const config = require("../../config");
const userService = require("../services/userService");
const authValidator = require("../validators/auth_validator");
const formValidators = require("../validators/form_validators");

// route handler
module.exports = {
  parameters: [
    {
      name: 'api_key',
      in: 'header',
      type: 'string',
      description: 'token',
    },
  ],
  get: GET,
  post: POST,
}

async function GET(req, res, next) {
  const users = await userService.getUsers();
  if (Object.keys(users).length != 0) {
    res.status(config.status_code.ok).send(users);
  } else {
    res.status(config.status_code.no_content).send();
  }
}

async function POST(req, res, next) {
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    if (formValidators.isValidData(req)) {
      if (formValidators.emailValidator(req.body.email)) {
        const query = await userService.addUser(req.body);
        console.log(query);
        if (query > 0) {
          res.status(config.status_code.created).send("New user added");
        } else {
          res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "User already exists" });
        }
      } else {
        res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Invalid email" });
      }
    } else {
      res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Invalid data" });
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

GET.apiDoc = {
  summary: "Fetch users.",
  operationId: "getUsers",
  tags: ['Users'],
  responses: {
    200: {
      description: "List of users.",
      schema: {
        type: "array",
        items: {
          $ref: "#/definitions/User",
        },
      },
    },
  },
};

POST.apiDoc = {
  summary: "Create new user.",
  operationId: "addUser",
  tags: ['Users'],
  consumes: ["application/json"],
  parameters: [
    {
      in: "body",
      name: "user",
      schema: {
        $ref: "#/definitions/User",
      },
    },
  ],
  responses: {
    201: {
      description: "Created",
    },
    400: {
      description: 'Something happened...',
      schema: {
        $ref: '#/definitions/ErrorResponse'
      }
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
};