const config = require("../../config");
const authValidator = require("../validators/auth_validator");
const jwt = require('jsonwebtoken');

module.exports = {
    parameters: [
        {
            name: 'refresh_token',
            in: 'header',
            type: 'string',
            description: 'token',
        },
    ],
    post: POST
}

async function POST(req, res, next) {
    const refreshToken = req.get("refresh_token");
    const user = req.body;
    console.log(user);
    if (authValidator.authenticateToken(refreshToken, config.token_type.refresh_token)) {
        if (Object.keys(user).length != 0) {
            const token = jwt.sign({ user }, config.app.accesstokensecret, { expiresIn: '1800s' });
            const currentDate = new Date().getTime();
            const accessExpiry = currentDate + 1800000;
            console.log("Token ", token);
            res.status(config.status_code.ok).send({
                "accessToken": token,
                "accessTokenExpiry": accessExpiry
            });
        } else {
            res.status(config.status_code.no_content).send();
        }
    }
}

POST.apiDoc = {
    summary: 'Refresh Token',
    operationId: 'refreshToken',
    tags: ['Token'],
    parameters: [
        {
            in: "body",
            name: "user",
            schema: {
                $ref: "#/definitions/User",
            },
        },
    ],
    responses: {
        200: {
            description: "Token refreshed",
        },
        400: {
            description: 'Something happened...',
            schema: {
                $ref: '#/definitions/ErrorResponse'
            }
        },
    },
    security: [
        {
            refreshToken: []
        }
    ]
}