import { useRouter } from "next/router";
import { useState } from "react";
import config from "../config";
import tokenWorker from "../store/token_worker";
import Card from "./card";
import RoleForm from "./roleForm";

export default function EditRoleLayout(props) {

    const userContext = props.context
    const [roleSearch, setRoleSearch] = useState('');
    const [searchData, setSearchData] = useState(null);

    const router = useRouter();

    if (!tokenWorker.getTokenWorker()) {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (localUser) {
            const url = new URL('../refreshtoken.js', import.meta.url)
            let worker = new Worker(url);
            worker.onmessage = function (event) {
                if (event.data) {
                    userContext.setUser(event.data);
                    tokenWorker.startTokenWorker(event.data);
                }
            }
            tokenWorker.setTokenWorker(worker);
            tokenWorker.startTokenWorker(localUser);
        }
    }

    window.onbeforeunload = (event) => {
        tokenWorker.terminateTokenWorker();
    }

    function searchBoxHandler(event) {
        const inputRoleName = event.target.value.trim() 
        setRoleSearch(inputRoleName)
        if (inputRoleName.length > 0) {
            if (!document.getElementById('searchboxspan').hidden) {
                document.getElementById('searchboxspan').hidden = true;
            }
        }
    }

    async function searchHandler(event) {
        event.preventDefault();
        if (roleSearch.length > 0) {
            const res = await fetch(`${config.app.baseUrl}/roles/${roleSearch}`, {
                method: 'GET',
                body: JSON.stringify(),
                headers: {
                    'Content-Type': 'application/json',
                    'api_key': userContext.user.accessToken
                }
            });
            if (res.status == '200') {
                const data = await res.json();
                console.log("Data retrieved is ", data[0]);
                setSearchData(data[0]);
                document.getElementById('roleform').style.display = 'block'
                document.getElementById('searchbtn').hidden = true
            } else {
                document.getElementById('roleform').style.display = 'none'
                alert(`Error ${res.status} ${res.statusText}`);
            }
        } else {
            document.getElementById('searchboxspan').hidden = false;
        }
    }

    function cancelHandler(event) {
        event.preventDefault();
        router.reload('/editrole')
    }

    return (
        <Card className='justify-center md:w-3/5 place-self-center'>
            <div className='flex flex-col'>
                <div className='md:text-5xl text-white pt-2'>
                    Edit User
                </div>
                <div id='searchboxdiv' className='flex flex-col pt-2'>
                    <label htmlFor='searchbox' className='text-white'>
                        Enter Role Name:
                    </label>
                    <input name='searchbox' id='searchbox' onChange={searchBoxHandler} />
                    <span id='searchboxspan' hidden={true} className='text-xs text-red-400'>
                        Enter Role Name
                    </span>
                    <button id='searchbtn' onClick={searchHandler} className='place-self-start mt-2 bg-blue-500 hover:bg-blue-700
                     text-white font-bold py-2 px-4 rounded'>Search</button>
                </div>
                <RoleForm id='roleform' mode='edit' onCancel={cancelHandler}
                    roledata={searchData} display='none' context={userContext} />
            </div>
        </Card>
    )
}