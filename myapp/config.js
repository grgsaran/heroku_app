/*Define constant variables*/
const config = {
    app: {
        //application constants
        port: 3001,
        accesstokensecret: 'pit-m3d',
        refreshtokensecret: 'refresh-pit-m3d'
    },
    db: {
        //database constants
        user: 'vuveislhyxibqu',
        host: 'ec2-18-235-4-83.compute-1.amazonaws.com',
        database: 'dc61brfpu32avl',
        password: 'd4dac55bc8eda525a60eef22f3274aecf76a7cd00a79e1392afae2143efd3503',
        port: '5432',
    },
    status_code:{
        //HTTP status codes
        ok: '200',
        created: '201',
        no_content: '204',
        not_modified: '304',
        bad_request: '400',
        unauthorized: '401',
        forbidden: '403',
        not_found: '404',
        conflict: '409',
    },
    token_type:{
        api_key: 'API_KEY',
        refresh_token: 'REFRESH_TOKEN'
    }
}
module.exports = config