import Head from "next/head";
import { useContext, useEffect, useState } from "react";
import EditRoleLayout from "../components/editRoleLayout";
import Layout from "../components/layout";
import UserContext from "../store/user_context";

export default function EditRole() {

    const userContext = useContext(UserContext)

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (!localUser) {
            window.location.href = '/login'
        } else {
            const currentTime = new Date().getTime();
            const tokenExpiry = new Date(localUser.refreshTokenExpiry).getTime();
            const timeDifference = tokenExpiry - currentTime;
            if (timeDifference < 0) {
                window.localStorage.removeItem('user')
                window.location.href = '/login'
            } else {
                if (localUser.r_name != 'Admin') {
                    window.location.href = '/'
                } else {
                    setIsLoading(false);
                }
            }
        }
    });

    if (isLoading) {
        return (
            <div>Loading...</div>
        )
    }

    return (
        <Layout>
            <Head>
                <title>Edit Role</title>
            </Head>
            <EditRoleLayout context={userContext} />
        </Layout>
    )
}