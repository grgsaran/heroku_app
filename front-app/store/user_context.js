import React from "react";

const UserContext = React.createContext({
    user: {},
    setUser: (user) => { },
    removeUser: () => { }
});

export default UserContext;