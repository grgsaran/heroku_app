import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import config from "../config"

export default function RoleForm(props) {

    const userCtx = props.context
    const router = useRouter();

    let roleData = {
        r_name: '',
        r_desc: '',
        r_permission: ''
    }

    if (props.roledata) {
        roleData = props.roledata
    }

    const mode = props.mode

    const [roleName, setRoleName] = useState('')
    const [roleDesc, setRoleDesc] = useState('')
    const [rolePerm, setRolePerm] = useState('')

    useEffect(() => {
        if (mode === 'edit') {
            console.log(roleData);
            setRoleName(roleData.r_name)
            setRoleDesc(roleData.r_desc)
            setRolePerm(roleData.r_permission)
        }
    }, [roleData]);

    function roleNameHandler(event) {
        setRoleName(event.target.value.trim())
    }

    function roleDescHandler(event) {
        setRoleDesc(event.target.value.trim())
    }

    function rolePermHandler(event) {
        setRolePerm(event.target.value.trim())
    }

    async function submitBtnHandler(event) {
        event.preventDefault();
        if (roleName.length > 0 && roleDesc.length > 0 && rolePerm.length > 0) {
            let url = `${config.app.baseUrl}/roles`;
            let method = 'POST';
            if (mode === 'edit') {
                url = `${config.app.baseUrl}/roles/${roleName}`
                method = 'PUT'
            }
            const res = await fetch(url, {
                method: method,
                body: JSON.stringify({
                    r_name: roleName,
                    r_desc: roleDesc,
                    r_permission: rolePerm
                }),
                headers: {
                    'Content-Type': 'application/json',
                    'api_key': userCtx.user.accessToken
                }
            });
            if (res.status == '201') {
                alert("Role added successfully")
                router.replace('/')
            } else if (res.status == '200') {
                alert("Role updated successfully")
                router.replace('/')
            } else {
                alert(`Error ${res.status}`)
            }
        } else {
            alert('Please enter all fields');
        }
    }

    return (
        <div id={props.id} style={{ display: props.display }} className='flex flex-col'>
            <div className='md:text-5xl text-white pt-3'>
                Enter Role Details
            </div>
            <span className='text-xs text-red-400'>
                All fields are mandatory
            </span>
            <div className='flex flex-col pt-3'>
                <label htmlFor='rolename' className='text-white'>Role Name</label>
                <input name='rolename' id='rolename' defaultValue={roleData.r_name}
                    onChange={roleNameHandler} className='bg-white mt-1' type='text' />
            </div>
            <div className='flex flex-col pt-3'>
                <label htmlFor='roledesc' className='text-white'>Description</label>
                <input name='roledesc' id='roledesc' defaultValue={roleData.r_desc}
                    onChange={roleDescHandler} className='bg-white mt-1' />
            </div>
            <div className='flex flex-col pt-3'>
                <label htmlFor='roleperm' className='text-white'>Permission</label>
                <input name='roleperm' id='roleperm' defaultValue={roleData.r_permission}
                    onChange={rolePermHandler} className='bg-white mt-1' />
            </div>

            <div className='flex pt-8 pb-3 justify-center'>
                <button id='submit' onClick={submitBtnHandler}
                    className='bg-blue-500 hover:bg-blue-700
                     text-white font-bold py-2 px-4 rounded'>
                    Submit
                </button>
                {mode === 'edit' && <button id='cancel' onClick={props.onCancel}
                    className='bg-blue-500 hover:bg-blue-700 text-white 
                    font-bold py-2 px-4 rounded ml-2'>
                    Cancel
                </button>}
            </div>
        </div>
    )
}