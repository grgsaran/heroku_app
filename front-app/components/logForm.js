import { useRouter } from "next/router";
import { useEffect, useState } from "react"
import config from "../config";
import validators from "../validators";

export default function LogForm(props) {

    const userCtx = props.context
    const mode = props.mode

    let logDetails = {
        login_time: '',
        logout_time: '',
        u_name: ''
    }

    if (props.logdetails) {
        logDetails = props.logdetails
    }

    const router = useRouter();

    const [username, setUsername] = useState('');
    const [loginTime, setLoginTime] = useState('');
    const [logoutTime, setLogoutTime] = useState('');

    useEffect(() => {
        if (mode === 'edit') {
            setUsername(logDetails.u_name) 
            setLoginTime(logDetails.login_time)
            setLogoutTime(logDetails.logout_time)
        }
    }, [logDetails])

    function usernameHandler(event) {
        setUsername(event.target.value.trim())
    }

    function loginTimeHandler(event) {
        const datetime = event.target.value.trim()
        setLoginTime(datetime)
        if (!validators.dateTimeValidator(datetime)) {
            document.getElementById('logintimespan').hidden = false;
        } else {
            document.getElementById('logintimespan').hidden = true;
        }
    }

    function logoutTimeHandler(event) {
        const datetime = event.target.value.trim()
        setLogoutTime(datetime)
        if (!validators.dateTimeValidator(datetime)) {
            document.getElementById('logouttimespan').hidden = false;
        } else {
            document.getElementById('logouttimespan').hidden = true;
        }
    }

    async function submitHandler(event) {
        event.preventDefault();
        console.log('Username ', username);
        console.log('Logintime ', loginTime);
        console.log('Logout ', logoutTime)
        if ((username.length > 0 && loginTime.length > 0 && logoutTime.length <= 0) ||
            (username.length > 0 && loginTime.length > 0 && logoutTime.length > 0)) {
            if (validators.dateTimeValidator(loginTime) && !validators.dateTimeValidator(logoutTime)) {
                const logindate = new Date(loginTime);
                await submitLog(username, logindate.toISOString(), '');
            } else if (validators.dateTimeValidator(loginTime) && validators.dateTimeValidator(logoutTime)) {
                const logindate = new Date(loginTime);
                const logoutdate = new Date(logoutTime);
                await submitLog(username, logindate.toISOString(), logoutdate.toISOString());
            }
        } else {
            alert('Please fill the required fields')
        }
    }

    async function submitLog(username, logintime, logouttime) {
        console.log(`Logintime ${logintime} logoutime ${logouttime}`);
        let url = `${config.app.baseUrl}/logs`
        let method = 'POST'
        if (mode && mode === 'edit'){
            url = `${config.app.baseUrl}/logs/${username}@${logDetails.login_time}`
            method = 'PUT'
        }
        const res = await fetch(url, {
            method: method,
            body: JSON.stringify({
                login_time: logintime,
                logout_time: logouttime,
                u_name: username
            }),
            headers: {
                'Content-Type': 'application/json',
                'api_key': userCtx.user.accessToken
            }
        });
        if(res.status == '200') {
            alert('Log updated successfully')
            router.replace('/')
        } else if (res.status == '201') {
            alert("Log added successfully")
            router.replace('/')
        } else {
            alert(`Error ${res.status} ${res.statusText}`)
        }
    }

    return (
        <div id={props.id} style={{ display: props.display }} className='flex flex-col'>
            <div className='md:text-5xl text-white pt-3'>
                Enter Log Details
            </div>
            <div className='flex flex-col pt-3'>
                <label htmlFor='username' className='text-white'>User Name</label>
                <span className='text-xs text-red-400'>
                    (Required)
                </span>
                <input name='username' id='username' onChange={usernameHandler}
                    className='bg-white mt-1' type='text' defaultValue={logDetails.u_name} />
            </div>
            <div className='flex flex-col pt-3'>
                <label htmlFor='logintime' className='text-white'>Login Time</label>
                <span className='text-xs text-red-400'>
                    (Required)
                </span>
                <input name='logintime' id='logintime' onChange={loginTimeHandler}
                    className='bg-white mt-1' placeholder='yyyy-mm-dd hh:mm:ss'
                    defaultValue={logDetails.login_time} />
                <span id='logintimespan' hidden={true} className='text-xs text-red-400'>
                    Enter valid date time format (yyyy-mm-dd hh:mm:ss)
                </span>
            </div>
            <div className='flex flex-col pt-3'>
                <label htmlFor='logouttime' className='text-white'>Logout Time</label>
                <span className='text-xs text-red-400'>
                    Leave blank if no date time
                </span>
                <input name='logouttime' id='logouttime' onChange={logoutTimeHandler}
                    className='bg-white mt-1' placeholder='yyyy-mm-dd hh:mm:ss'
                    defaultValue={logDetails.logout_time} />
                <span id='logouttimespan' hidden={true} className='text-xs text-red-400'>
                    Enter valid date time format (yyyy-mm-dd hh:mm:ss)
                </span>
            </div>

            <div className='flex pt-8 pb-3 justify-center'>
                <button id='submit' onClick={submitHandler}
                    className='bg-blue-500 hover:bg-blue-700
                     text-white font-bold py-2 px-4 rounded'>
                    Submit
                </button>
                {mode === 'edit' && <button id='cancel' onClick={props.onCancel}
                    className='bg-blue-500 hover:bg-blue-700 text-white 
                    font-bold py-2 px-4 rounded ml-2'>
                    Cancel
                </button>}
            </div>
        </div>
    )
}