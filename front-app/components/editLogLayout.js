import { useRouter } from "next/router"
import { useState } from "react"
import config from "../config"
import tokenWorker from "../store/token_worker"
import Card from "./card"
import LogForm from "./logForm"

export default function EditLogLayout(props) {

    const userCtx = props.context
    const router = useRouter();

    const [searchUser, setSearchUser] = useState('')
    const [searchData, setSearchData] = useState(null)
    const [selectedLog, setSlelectedLog] = useState(null);

    if (!tokenWorker.getTokenWorker()) {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (localUser) {
            const url = new URL('../refreshtoken.js', import.meta.url)
            let worker = new Worker(url);
            worker.onmessage = function (event) {
                if (event.data) {
                    console.log('On Message ', event.data);
                    userCtx.setUser(event.data);
                    tokenWorker.startTokenWorker(event.data);
                }
            }
            tokenWorker.setTokenWorker(worker);
            tokenWorker.startTokenWorker(localUser);
        }
    }

    window.onbeforeunload = (event) => {
        tokenWorker.terminateTokenWorker();
    }

    function userSearchBoxHandler(event) {
        const inputUser = event.target.value.trim()
        setSearchUser(inputUser)
        if (inputUser.length > 0) {
            document.getElementById('usersearchboxspan').hidden = true;
        } else {
            document.getElementById('usersearchboxspan').hidden = false;
        }
    }

    function formatDate(date) {
        if (date && date.length > 0) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear(),
                hour = d.getHours(),
                minute = d.getMinutes(),
                second = d.getSeconds();
            
            console.log("New date is ",d);

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            if (hour.length < 2)
                hour = '0' + hour;
            if (minute.length < 2)
                minute = '0' + minute;
            if (second.length < 2)
                second = '0' + second;

            return [year, month, day].join('-') + ' ' + [hour, minute, second].join(':');
        } else {
            return ''
        }
    }

    function clearlist() {
        const length = document.getElementById('logs').childNodes.length;
        for (let i = length - 1; i >= 1; i--) {
            document.getElementById('logs').remove(i);
            console.log(i);
        }
    }

    async function searchHandler(event) {
        event.preventDefault();
        if (searchUser.length > 0) {
            const res = await fetch(`${config.app.baseUrl}/logs/${searchUser}`, {
                method: 'GET',
                body: JSON.stringify(),
                headers: {
                    'Content-Type': 'application/json',
                    'api_key': userCtx.user.accessToken
                }
            });
            if (res.status == '200') {
                const data = await res.json();
                console.log('Logs are ', data);
                setSearchData(data)
                if (document.getElementById('logs').childNodes.length > 1) {
                    clearlist();
                }
                data.forEach(log => {
                    let option = document.createElement('option')
                    option.value = log.login_time
                    option.innerHTML = formatDate(log.login_time)
                    document.getElementById('logs').appendChild(option)
                });
                document.getElementById('logs').hidden = false;
            } else if (res.status == '204') {
                alert(`No logs found`)
            } else {
                alert(`Error ${res.status} ${res.statusText}`)
            }
        } else {
            alert('Enter User Name')
        }
    }

    function onLogSelected(event) {
        const selectedDate = event.target.value
        if (selectedDate.length > 0) {
            console.log(selectedDate);
            searchData.forEach(data => {
                if (data.login_time === selectedDate) {
                    let sendingData = {}
                    sendingData.login_time = formatDate(data.login_time)
                    sendingData.logout_time = formatDate(data.logout_time)
                    sendingData.u_name = searchUser
                    setSlelectedLog(sendingData)
                    document.getElementById('searchbtn').hidden = true
                    document.getElementById('usersearchbox').setAttribute('disabled', true)
                    document.getElementById('logs').setAttribute('disabled', true)
                    document.getElementById('logform').style.display = 'block'
                    return
                }
            })
        }
    }

    function cancelHandler(event) {
        event.preventDefault();
        router.reload('/editlog');
    }

    return (
        <Card className='justify-center md:w-3/5 place-self-center'>
            <div className='flex flex-col'>
                <div className='md:text-5xl text-white pt-2'>
                    Edit Log
                </div>
                <div id='searchboxdiv' className='flex flex-col pt-2'>
                    <label htmlFor='usersearchbox' className='text-white'>
                        Enter Username:
                    </label>
                    <input name='usersearchbox' id='usersearchbox' onChange={userSearchBoxHandler} />
                    <span id='usersearchboxspan' hidden={true} className='text-xs text-red-400'>
                        You need to enter username
                    </span>
                    <button id='searchbtn' onClick={searchHandler} className='place-self-start mt-2 bg-blue-500 hover:bg-blue-700
                     text-white font-bold py-2 px-4 rounded'>Search</button>
                    <select id='logs' hidden={true} onChange={onLogSelected} className='mt-2 p-1'>
                        <option value=''>Select time</option>
                    </select>
                    <LogForm id='logform' context={userCtx} mode='edit'
                        display='none' onCancel={cancelHandler}
                        formatdate={formatDate} logdetails={selectedLog} />
                </div>
            </div>
        </Card>
    )
}