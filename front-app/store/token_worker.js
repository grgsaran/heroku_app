let worker = null;
const tokenWorker = {
    setTokenWorker(paramWorker) {
        console.log("Worker set");
        worker = paramWorker;
    },
    getTokenWorker() {
        return worker;
    },
    startTokenWorker(msg) {
        if (worker) {
            worker.postMessage(msg);
            console.log("Worker started");
        }
    },
    terminateTokenWorker() {
        if (worker) {
            worker.terminate();
            worker = null;
            console.log("Worker terminated");
        }
    }
}

module.exports = tokenWorker;