const jwt = require('jsonwebtoken');
const config = require('../../config');

const authValidator = {
  authenticateToken(token, tokenType) {
    let value = false;
    console.log("Authenticate ", token);
    if (!token) {
      return value;
    }
    if (tokenType === config.token_type.api_key) {
      jwt.verify(token, config.app.accesstokensecret, (err, decode) => {
        console.log("JWT Error ", err);
        if (err) {
          return;
        }
        value = true;
      })
    } else if (tokenType == config.token_type.refresh_token) {
      jwt.verify(token, config.app.refreshtokensecret, (err, decode) => {
        console.log("JWT Error ", err);
        if (err) {
          return;
        }
        value = true;
      })
    }
    return value;
  }
};

module.exports = authValidator;