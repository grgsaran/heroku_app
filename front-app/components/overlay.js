import Link from "next/link"
import { useEffect } from "react";

export default function Overlay(props) {
    const overlayName = props.name;
    const overlayHandler = props.overLayHandler;

    return (
        <div className=' bg-gray-900 absolute w-screen h-screen opacity-90'>
            <div className='mt-10 flex items-center justify-center'>
                <div className='flex flex-col w-3/4 md:w-1/4 text-yellow-600 bg-white'>
                    <div className='flex justify-end'>
                        <button className=' text-yellow-600 bg-transparent' onClick={overlayHandler}>Close</button>
                    </div>
                    <div className='flex justify-center mt-3'>
                        <Link href={props.addHref}>
                            <a>Add {overlayName}</a>
                        </Link>
                    </div>
                    <div className='flex justify-center mt-3'>
                        <Link href={props.editHref}>
                            <a>Edit {overlayName}</a>
                        </Link>
                    </div>
                    <div className='flex justify-center mt-3 mb-3'>
                        <Link href={props.deleteHref}>
                            <a>Remove {overlayName}</a>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}