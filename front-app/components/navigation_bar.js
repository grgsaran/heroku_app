import logo from "../public/images/PITM3D.png";
import Image from 'next/image'
import Link from 'next/link'
import { useContext } from "react";
import UserContext from "../store/user_context";
import { useRouter } from 'next/router'
import config from "../config";

export default function NavigationBar({ children }) {

    const userCtx = useContext(UserContext);
    const router = useRouter();

    function formatDate(date) {
        if (date && date.length > 0) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear(),
                hour = d.getHours(),
                minute = d.getMinutes(),
                second = d.getSeconds();

            console.log("New date is ", d);

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            if (hour.length < 2)
                hour = '0' + hour;
            if (minute.length < 2)
                minute = '0' + minute;
            if (second.length < 2)
                second = '0' + second;

            return [year, month, day].join('-') + ' ' + [hour, minute, second].join(':');
        } else {
            return ''
        }
    }

    async function onLogout(event) {
        event.preventDefault();

        console.log(userCtx.user.u_name);
        const formatLoginTime = formatDate(userCtx.user.loginDate)
        const currentTime = new Date();
        const url = `${config.app.baseUrl}/logs/${userCtx.user.u_name}@${userCtx.user.loginDate}`
        const method = 'PUT'
        const res = await fetch(url, {
            method: method,
            body: JSON.stringify({
                login_time: formatLoginTime,
                logout_time: currentTime.toISOString(),
                u_name: userCtx.user.u_name
            }),
            headers: {
                'Content-Type': 'application/json',
                'api_key': userCtx.user.accessToken
            }
        });
        if (res.status == '200') {
            userCtx.removeUser();
            router.replace('/');
        } else {
            alert(`Error ${res.status} ${res.statusText}`)
        }

    }

    return (
        <nav className='bg-indigo-900 w-screen'>
            <div className='max-w-7xl mx-auto px-2 sm:px-6 lg:px-8 shadow-2xl'>
                <div className="relative flex justify-between h-16">
                    <div className='flex-shrink flex'>
                        <Link href='/'>
                            <a className='flex'>
                                <Image className=' object-contain' src={logo} alt='Home' />
                            </a>
                        </Link>
                    </div>

                    {userCtx.user.u_name &&
                        <div className="flex place-items-center origin-right h-16">
                            <Link href='/profile'>
                                <a className='flex text-white mr-5'>Profile</a>
                            </Link>
                            <button className='text-white' onClick={onLogout}>Logout</button>
                        </div>
                    }
                </div>
            </div>
        </nav>
    )
}