import { useRouter } from "next/router"
import { useState } from "react"
import config from "../config"
import tokenWorker from "../store/token_worker"
import Card from "./card"
import UserForm from "./userForm"

export default function UserEditLayout(props) {

    const userCtx = props.context;

    const router = useRouter();

    const [searchId, setSearchId] = useState('');
    const [searchData, setSearchData] = useState(null);

    if (!tokenWorker.getTokenWorker()) {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (localUser) {
            const url = new URL('../refreshtoken.js', import.meta.url)
            let worker = new Worker(url);
            worker.onmessage = function (event) {
                if (event.data) {
                    userCtx.setUser(event.data);
                    tokenWorker.startTokenWorker(event.data);
                }
            }
            tokenWorker.setTokenWorker(worker);
            tokenWorker.startTokenWorker(localUser);
        }
    }

    window.onbeforeunload = (event) => {
        tokenWorker.terminateTokenWorker();
    }

    function searchIdBoxHandler(event) {
        const searchitem = event.target.value
        setSearchId(searchitem.trim())
        if (searchitem.trim().length > 0) {
            if (!document.getElementById('searchboxspan').hidden) {
                document.getElementById('searchboxspan').hidden = true;
            }
        }
    }

    async function searchHandler(event) {
        event.preventDefault();
        if (searchId.length > 0) {
            const res = await fetch(`${config.app.baseUrl}/users/${searchId}`, {
                method: 'GET',
                body: JSON.stringify(),
                headers: {
                    'Content-Type': 'application/json',
                    'api_key': userCtx.user.accessToken
                }
            });
            if (res.status == '200') {
                const data = await res.json();
                console.log("Data retrieved is ", data[0]);
                setSearchData(data[0]);
                document.getElementById('userform').style.display = 'block'
                document.getElementById('searchbtn').hidden = true
            } else {
                document.getElementById('userform').style.display = 'none'
                alert(`Error ${res.status} ${res.statusText}`);
            }
        } else {
            document.getElementById('searchboxspan').hidden = false;
        }
    }

    function cancelHandler(event) {
        event.preventDefault();
        router.reload('/edituser')
    }

    return (
        <Card className='justify-center md:w-3/5 place-self-center'>
            <div className='flex flex-col'>
                <div className='md:text-5xl text-white pt-2'>
                    Edit User
                </div>
                <div id='searchboxdiv' className='flex flex-col pt-2'>
                    <label htmlFor='searchbox' className='text-white'>
                        Enter User ID:
                    </label>
                    <input name='searchbox' id='searchbox' onChange={searchIdBoxHandler} />
                    <span id='searchboxspan' hidden={true} className='text-xs text-red-400'>
                        Enter search ID
                    </span>
                    <button id='searchbtn' onClick={searchHandler} className='place-self-start mt-2 bg-blue-500 hover:bg-blue-700
                     text-white font-bold py-2 px-4 rounded'>Search</button>
                </div>
                <UserForm id='userform' mode='edit' onCancel={cancelHandler}
                    userdata={searchData} display='none' context={userCtx}
                    roles={props.roles} />
            </div>
        </Card>
    )
}