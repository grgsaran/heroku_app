const userService = require("../services/userService");
const config = require("../../config");
const jwt = require('jsonwebtoken');
const logService = require("../services/logService");

module.exports = {
  post: POST,
};

async function POST(req, res, next) {
  console.log(`username ${req.body.user_name}`);
  const users = await userService.loginUser(req.body.user_name, req.body.password);
  if (Object.keys(users).length != 0) {
    console.log("Inside if ", users);
    let currentLog = {}
    const currentLogDate = new Date().toISOString();
    currentLog.login_time = currentLogDate;
    currentLog.logout_time = '';
    currentLog.u_name = req.body.user_name
    await logService.addLog(currentLog)
    const token = jwt.sign({ users }, config.app.accesstokensecret, { expiresIn: '1800s' });
    const refreshToken = jwt.sign({ users }, config.app.refreshtokensecret, { expiresIn: '604800s' });
    console.log("Token: ", token);
    console.log("Refresh Token: ", refreshToken);
    users[0].accessToken = token;
    const currentDate = new Date().getTime();
    const accessExpiry = currentDate + 1800000;
    users[0].accessTokenExpiry = accessExpiry;
    users[0].refreshToken = refreshToken;
    const refreshExpiry = currentDate + 604800000;
    users[0].refreshTokenExpiry = refreshExpiry;
    users[0].loginDate = currentLogDate;
    res.status(config.status_code.ok).send(users);
  } else {
    res.status(config.status_code.no_content).send();
  }
}

POST.apiDoc = {
  summary: "User details on login.",
  operationId: "loginUser",
  tags: ['Users'],
  parameters: [
    {
      in: "body",
      name: "user",
      schema: {
        $ref: "#/definitions/Auth",
      },
    },
  ],
  responses: {
    200: {
      description: "User.",
      schema: {
        type: "object",
        items: {
          $ref: "#/definitions/User",
        },
      },
    },
  },
};