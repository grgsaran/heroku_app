import Head from "next/head";
import Layout from "../components/layout";
import LoginForm from "../components/loginForm";

export default function Login() {

    return (
        <Layout>
            <Head>
                <title>Login</title>
            </Head>
            <LoginForm/>
        </Layout>
    )
}