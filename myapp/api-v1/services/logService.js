const dbCon = require('../../db_connection')

const logService = {
    async getLogs() {
        let result;
        await dbCon.returnDB().query('SELECT login_time, logout_time, u_name from logs, users WHERE logs.u_id = users.u_id')
            .then((query) => {
                result = query;
                console.log(result.rows);
            }).catch((e) => {
                console.log(e);
            });
        if (result && result.rowCount) {
            return result.rows;
        } else {
            return {};
        }
    },

    async getLogsByUid(u_name) {
        let result;
        await dbCon.returnDB().query('SELECT login_time, logout_time from logs where u_id = (SELECT u_id from users WHERE u_name = $1)', [u_name])
            .then((query) => {
                result = query;
                console.log(result.rows);
            }).catch((e) => {
                console.log(e);
            });
        if (result && result.rowCount) {
            return result.rows;
        } else {
            return {};
        }
    },

    async addLog(req) {
        const { login_time, logout_time, u_name } = req;
        console.log(req.login_time);
        let result;
        if (login_time && logout_time.length > 0) {
            await dbCon.returnDB().query('INSERT INTO logs(login_time, logout_time, u_id) VALUES($1, $2, (SELECT u_id from users WHERE u_name = $3))', [login_time, logout_time, u_name])
                .then((query) => {
                    console.log(`Query count ${query.rowCount}`);
                    result = query;
                }).catch((e) => {
                    console.log(`Error catch ${e}`);
                });
        } else {
            await dbCon.returnDB().query('INSERT INTO logs(login_time, u_id) VALUES($1, (SELECT u_id from users WHERE u_name = $2))', [login_time, u_name])
                .then((query) => {
                    console.log(`Query count ${query.rowCount}`);
                    result = query;
                }).catch((e) => {
                    console.log(`Error catch ${e}`);
                });
        }
        if (result && result.rowCount) {
            return result.rowCount;
        } else {
            return 0;
        }
    },

    async updateLog(req, param_u_name, param_login_time) {
        const { login_time, logout_time, u_name } = req;
        let result;
        if (logout_time && logout_time.length > 0) {
            await dbCon.returnDB().query('UPDATE logs set login_time = $1, logout_time = $2, u_id = (SELECT u_id from users WHERE u_name = $3) WHERE u_id = (SELECT u_id from users WHERE u_name = $4) AND login_time = $5', [login_time, logout_time, u_name, param_u_name, param_login_time])
                .then((query) => {
                    console.log(`Query count ${query.rowCount}`);
                    result = query;
                }).catch((e) => {
                    console.log(`Error catch ${e}`);
                });
        } else {
            await dbCon.returnDB().query('UPDATE logs set login_time = $1, u_id = (SELECT u_id from users WHERE u_name = $2) WHERE u_id = (SELECT u_id from users WHERE u_name = $3) AND login_time = $4', [login_time, u_name, param_u_name, param_login_time])
                .then((query) => {
                    console.log(`Query count ${query.rowCount}`);
                    result = query;
                }).catch((e) => {
                    console.log(`Error catch ${e}`);
                });
        }
        if (result && result.rowCount) {
            return result.rowCount;
        } else {
            return 0;
        }
    },

    async deleteLogByUid(u_name) {
        let result;
        await dbCon.returnDB().query('DELETE from logs WHERE u_id = (SELECT u_id from users WHERE u_name = $1)', [u_name])
            .then((query) => {
                console.log(`Query count ${query.rowCount}`);
                result = query;
            }).catch((e) => {
                console.log(`Error catch ${e}`);
            });
        if (result && result.rowCount) {
            return result.rowCount;
        } else {
            return 0;
        }
    },

    async deleteLogByUidTime(u_name, login_time) {
        let result;
        await dbCon.returnDB().query('DELETE from logs WHERE u_id = (SELECT u_id from users WHERE u_name = $1) AND login_time = $2', [u_name, login_time])
            .then((query) => {
                console.log(`Query count ${query.rowCount}`);
                result = query;
            }).catch((e) => {
                console.log(`Error catch ${e}`);
            });
        if (result && result.rowCount) {
            return result.rowCount;
        } else {
            return 0;
        }
    }
}
module.exports = logService;