//schema definition
const apiDoc = {
  swagger: "2.0",
  basePath: "/",
  info: {
    title: "User management API.",
    version: "1.0.0",
  },
  definitions: {
    User: {
      type: "object",
      properties: {
        u_name: {
          type: "string",
        },
        email: {
          type: "string",
          format: "email"
        },
        password: {
          type: "string",
        },
        f_name: {
          type: "string",
        },
        l_name: {
          type: "string",
        },
        r_name: {
          type: "string",
          description: "Role name"
        },
      },
      required: ["u_name", "email", "password", "f_name", "l_name", "r_name"],
    },
    Role: {
      type: "object",
      properties: {
        r_name: {
          type: "string",
        },
        r_desc: {
          type: "string",
        },
        r_permission: {
          type: "string",
        },
      },
      required: ["r_name", "r_desc", "r_permission"],
    },
    Log: {
      type: "object",
      properties: {
        login_time: {
          type: "string",
          description: "ISO format",
          format: "date-time"
        },
        logout_time: {
          type: 'string',
          description: "ISO format",
          format: "date-time",
          default: ""
        },
        u_name: {
          type: "string",
          description: "User name"
        },
      },
      required: ["login_time", "logout_time", "u_name"],
    },
    Auth: {
      type: "object",
      properties: {
        user_name: {
          type: "string",
          description: "User name"
        },
        password: {
          type: "string",
          description: "Password",
          format: "password"
        },
      },
      required: ["user_name", "password"],
    },
    ErrorResponse: {
      type: "object",
      properties: {
        errorCode: {
          type: "string"
        },
        message: {
          type: "string"
        }
      }
    },
  },
  paths: {},
  securityDefinitions: {
    keyScheme: {
      type: 'apiKey',
      name: 'api_key',
      in: 'header'
    },
    refreshToken: {
      type: 'apiKey',
      name: 'refresh_token',
      in: 'header'
    }
  },
  tags: [
    { description: "User operations", name: "Users" },
    { description: "Role operations", name: "Roles" },
    { description: "Log operations", name: "Logs" }
  ],
};

module.exports = apiDoc;