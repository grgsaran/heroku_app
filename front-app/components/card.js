export default function Card(props) {
    const classes = "flex flex-wrap origin-top-center md:p-3 shadow-2xl border rounded-xl bg-indigo-900 md:m-8 "
        + props.className;

    const onPress = props.onPress;
    const id = props.id;
    return <div id={id} className={classes} onClick={onPress}>{props.children}</div>
}