const logService = require("../../services/logService");
const config = require("../../../config");
const authValidator = require("../../validators/auth_validator");

module.exports = {
  parameters: [
    {
      name: 'id',
      in: 'path',
      type: 'string',
      required: true,
      description: 'User name',
    },
    {
      name: 'time',
      in: 'path',
      type: 'string',
      required: true,
      description: 'Login time',
    },
    {
      name: 'api_key',
      in: 'header',
      type: 'string',
      description: 'token',
    },
  ],
  put: PUT,
  delete: DELETE,
};

async function PUT(req, res, next) {
  console.log(req.params.id);
  console.log(req.params.time);
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    if (req.body.u_name.length > 0) {
      const query = await logService.updateLog(req.body, req.params.id, req.params.time);
      console.log(query);
      if (query > 0) {
        res.status(config.status_code.ok).send("Log updated");
      } else {
        res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Cannot update log" });
      }
    } else {
      res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Invalid data" });
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

async function DELETE(req, res, next) {
  console.log(`About to delete log for user: ${req.params.id}`);
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    const query = await logService.deleteLogByUidTime(req.params.id, req.params.time);
    console.log(query);
    if (query > 0) {
      res.status(config.status_code.ok).send(`Logs deleted for user ${req.params.id}`);
    } else {
      res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Logs not deleted." });
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

PUT.apiDoc = {
  summary: "Update log.",
  operationId: "updateLog",
  tags: ['Logs'],
  parameters: [
    {
      in: "body",
      name: "log",
      schema: {
        $ref: "#/definitions/Log",
      },
    },
  ],
  responses: {
    200: {
      description: "Updated ok",
    },
    400: {
      description: 'Something happened...',
      schema: {
        $ref: '#/definitions/ErrorResponse'
      }
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
}

DELETE.apiDoc = {
  summary: "Delete log.",
  operationId: "deleteLog",
  tags: ['Logs'],
  consumes: ["application/json"],
  responses: {
    200: {
      description: "Delete",
    },
    400: {
      description: 'Something happened...',
      schema: {
        $ref: '#/definitions/ErrorResponse'
      }
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
}