const roleService = require("../../services/roleService");
const config = require("../../../config");
const authValidator = require("../../validators/auth_validator");

module.exports = {
  parameters: [
    {
      name: 'id',
      in: 'path',
      type: 'string',
      required: true,
      description: 'Role name',
    },
    {
      name: 'api_key',
      in: 'header',
      type: 'string',
      description: 'token',
    },
  ],
  get: GET,
  put: PUT,
  delete: DELETE,
};

async function GET(req, res, next) {
  console.log(`rolename ${req.params.id}`);
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    const roles = await roleService.getRoleById(req.params.id);
    if (Object.keys(roles).length != 0) {
      res.status(config.status_code.ok).send(roles);
    } else {
      res.status(config.status_code.no_content).send();
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

async function PUT(req, res, next) {
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    if (req.body.r_name.length > 0 && req.body.r_desc.length > 0) {
      const query = await roleService.updateRole(req.params.id, req.body);
      console.log(query);
      if (query > 0) {
        res.status(config.status_code.ok).send("Role updated");
      } else {
        res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Role already exists" });
      }
    } else {
      res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Invalid data" });
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

async function DELETE(req, res, next) {
  console.log(`About to delete role name: ${req.params.id}`);
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    const query = await roleService.deleteRole(req.params.id);
    console.log(query);
    if (query > 0) {
      res.status(config.status_code.ok).send("User deleted");
    } else {
      res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Role name not found" });
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

GET.apiDoc = {
  summary: "Fetch roles by role name.",
  operationId: "getRolesById",
  tags: ['Roles'],
  responses: {
    200: {
      description: "Role.",
      schema: {
        type: "object",
        items: {
          $ref: "#/definitions/Role",
        },
      },
    },
    204: {
      description: "No content found."
    }
  },
  security: [
    {
      keyScheme: []
    }
  ]
};

PUT.apiDoc = {
  summary: "Update role.",
  operationId: "updateRole",
  tags: ['Roles'],
  parameters: [
    {
      in: "body",
      name: "role",
      schema: {
        $ref: "#/definitions/Role",
      },
    },
  ],
  responses: {
    200: {
      description: "Updated ok",
    },
    400: {
      description: 'Something happened...',
      schema: {
        $ref: '#/definitions/ErrorResponse'
      }
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
};

DELETE.apiDoc = {
  summary: "Delete role.",
  operationId: "deleteRole",
  tags: ['Roles'],
  consumes: ["application/json"],
  responses: {
    200: {
      description: "Delete",
    },
    400: {
      description: 'Something happened...',
      schema: {
        $ref: '#/definitions/ErrorResponse'
      }
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
};