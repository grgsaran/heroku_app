import Card from "./card"
import { useState } from "react";
import Overlay from "./overlay";
import tokenWorker from "../store/token_worker";

export default function Dashboard(props) {

    const userCtx = props.context;

    if (!tokenWorker.getTokenWorker()) {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (localUser) {
            const url = new URL('../refreshtoken.js', import.meta.url)
            let worker = new Worker(url);
            worker.onmessage = function (event) {
                if (event.data) {
                    userCtx.setUser(event.data);
                    tokenWorker.startTokenWorker(event.data);
                }
            }
            tokenWorker.setTokenWorker(worker);
            tokenWorker.startTokenWorker(localUser);
        }
    }

    const [overlayView, setOverLay] = useState(false);
    const [overlayName, setOverLayName] = useState('');
    const [addHref, setAddHref] = useState('');
    const [editHref, setEditHref] = useState('');
    const [deleteHref, setDeleteHref] = useState('');

    console.log("Overlay is ", overlayView);

    function overLayHandler(event) {
        console.log("Event ", event.target.id);
        const id = event.target.id
        if(id === 'User'){
            setAddHref('/adduser')
            setEditHref('/edituser')
            setDeleteHref('/deleteuser')
        } else if(id === 'Role'){
            setAddHref('/addrole')
            setEditHref('/editrole')
            setDeleteHref('/deleterole')
        } else if(id === 'Log'){
            setAddHref('/addlog')
            setEditHref('/editlog')
            setDeleteHref('/deletelog')
        }
        setOverLayName(id);
        setOverLay(!overlayView);
    }

    window.onbeforeunload = (event) => {
        tokenWorker.terminateTokenWorker();
    }

    return (
        <div className='flex flex-col w-screen h-screen'>
            {overlayView && (<Overlay name={overlayName} addHref={addHref} 
            editHref={editHref} deleteHref={deleteHref} overLayHandler={overLayHandler} />)}
            <div className='flex flex-row'>
                <div className='flex flex-none justify-end w-1/2'>
                    <Card id='User' onPress={overLayHandler} className='hover:bg-indigo-800 justify-center w-3/4'>
                        <div id='User' className='flex flex-col font-sans text-white p-1'>
                            <div id='User' className='flex flex-wrap md:text-7xl justify-center'>
                                Users
                            </div>
                            <span id='User' className='flex flex-wrap text-xs justify-center md:m-2' >
                                Create, edit and delete users
                            </span>
                        </div>
                    </Card>
                </div>
                <div className='flex justify-start w-1/2'>
                    <Card id='Role' onPress={overLayHandler} className='hover:bg-indigo-800 justify-center w-3/4'>
                        <div id='Role' className='flex flex-col font-sans text-white p-1'>
                            <div id='Role' className='flex flex-wrap md:text-7xl justify-center'>
                                Roles
                            </div>
                            <span id='Role' className='flex flex-wrap text-xs justify-center md:m-2' >
                                Create, edit and delete roles
                            </span>
                        </div>
                    </Card>
                </div>
            </div>
            <div className='flex flex-wrap justify-center'>
                <Card id='Log' onPress={overLayHandler} className='hover:bg-indigo-800 justify-center w-1/2'>
                    <div id='Log' className='flex flex-col font-sans text-white p-1'>
                        <div id='Log' className='flex flex-wrap md:text-7xl justify-center'>
                            Logs
                        </div>
                        <span id='Log' className='flex flex-wrap text-xs justify-center md:m-2' >
                            Create, edit and delete logs
                        </span>
                    </div>
                </Card>
            </div>
        </div>
    )
}