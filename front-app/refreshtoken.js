onmessage = function (event) {
    const user = event.data;
    const currentTime = new Date().getTime();
    const tokenExpiry = new Date(user.accessTokenExpiry).getTime();
    const timeDifference = tokenExpiry - currentTime;
    let timeOutMilliSeconds = 1000;
    if (timeDifference > 60000) {
        timeOutMilliSeconds = timeDifference;
    }
    const timer = setTimeout(async () => {
        clearTimeout(timer);
        const res = await fetch(`https://damp-sierra-29447.herokuapp.com/refreshToken`, {
            mode: 'cors',
            method: 'POST',
            body: JSON.stringify({
                u_name: user.u_name,
                email: user.email,
                password: user.password,
                f_name: user.f_name,
                l_name: user.l_name,
                r_name: user.r_name
            }),
            headers: {
                'Content-Type': 'application/json',
                'refresh_token': user.refreshToken
            }
        });
        if (res.status == '200') {
            const data = await res.json();
            user.accessToken = data.accessToken;
            user.accessTokenExpiry = data.accessTokenExpiry;
            postMessage(user);
        } else {
            postMessage(null);
        }
    }, timeOutMilliSeconds);
    return;
}