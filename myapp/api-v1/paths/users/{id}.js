const config = require("../../../config");
const userService = require("../../services/userService");
const authValidator = require("../../validators/auth_validator");
const formValidators = require("../../validators/form_validators");

module.exports = {
  parameters: [
    {
      name: 'id',
      in: 'path',
      type: 'string',
      required: true,
      description: 'User name',
    },
    {
      name: 'api_key',
      in: 'header',
      type: 'string',
      description: 'token',
    },
  ],
  get: GET,
  put: PUT,
  delete: DELETE,
};

async function GET(req, res, next) {
  console.log(`username ${req.params.id}`);
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    const users = await userService.getUsersById(req.params.id);
    if (Object.keys(users).length != 0) {
      res.status(config.status_code.ok).send(users);
    } else {
      res.status(config.status_code.no_content).send();
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

async function PUT(req, res, next) {
  console.log(`About to update user name: ${req.params.id}`);
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    if (formValidators.isValidData(req) || formValidators.isValidDataNoPassword(req)) {
      if (formValidators.emailValidator(req.body.email)) {
        const query = await userService.updateUser(req.params.id, req.body);
        console.log(query);
        if (query > 0) {
          res.status(config.status_code.ok).send("User updated");
        } else {
          res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "User not updated. Username already exists." });
        }
      } else {
        res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Invalid email" });
      }
    } else {
      res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Invalid data" });
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

async function DELETE(req, res, next) {
  const token = req.get("api_key");
  console.log(`About to delete user name: ${req.params.id}`);
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    const query = await userService.deleteUser(req.params.id);
    console.log(query);
    if (query > 0) {
      res.status(config.status_code.ok).send("User deleted");
    } else {
      res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "User name not found" });
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

GET.apiDoc = {
  summary: "Fetch users by ID.",
  operationId: "getUsersById",
  tags: ['Users'],
  responses: {
    200: {
      description: "User.",
      schema: {
        type: "object",
        items: {
          $ref: "#/definitions/User",
        },
      },
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
};

PUT.apiDoc = {
  summary: "Update user.",
  operationId: "updateUser",
  tags: ['Users'],
  parameters: [
    {
      in: "body",
      name: "user",
      schema: {
        $ref: "#/definitions/User",
      },
    },
  ],
  responses: {
    200: {
      description: "Updated ok",
    },
    400: {
      description: 'Something happened...',
      schema: {
        $ref: '#/definitions/ErrorResponse'
      }
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
};

DELETE.apiDoc = {
  summary: "Delete user.",
  operationId: "deleteUser",
  tags: ['Users'],
  consumes: ["application/json"],
  responses: {
    200: {
      description: "Delete",
    },
    400: {
      description: 'Something happened...',
      schema: {
        $ref: '#/definitions/ErrorResponse'
      }
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
};