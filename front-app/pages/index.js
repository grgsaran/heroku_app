import Head from 'next/head'
import Layout from '../components/layout'
import { useContext, useEffect, useState } from 'react';
import Dashboard from '../components/dashboard';
import UserContext from '../store/user_context';

export default function Home({ props }) {

  const usrCtx = useContext(UserContext);
  console.log("User context is ", usrCtx);

  const [isLoading, setIsLoading] = useState(true);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    const localUser = JSON.parse(window.localStorage.getItem('user'))
    if (!localUser) {
      window.location.href = '/login'
    } else {
      const currentTime = new Date().getTime();
      const tokenExpiry = new Date(localUser.refreshTokenExpiry).getTime();
      const timeDifference = tokenExpiry - currentTime;
      if (timeDifference < 0) {
        window.localStorage.removeItem('user')
        window.location.href = '/login'
      } else {
        setIsLoading(false);
        if (localUser.r_name == 'Admin') {
          setIsAdmin(true);
        }
      }
    }
  });

  if (isLoading) {
    return (
      <div>Loading...</div>
    )
  }

  return (
    <Layout>
      <Head>
        <title>PIT-M3D</title>
      </Head>
      {isAdmin && <Dashboard context={usrCtx} />}
      {!isAdmin && <div className='flex flex-wrap place-content-center'>Welcome to PIT-M3D</div>}
    </Layout>
  )
}