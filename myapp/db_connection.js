/*Establish db connection with postgresql*/
const config = require('./config')
//Configuring connection for database pool
const Pool = require('pg').Pool
const pool = new Pool({
    user: config.db.user,
    host: config.db.host,
    database: config.db.database,
    password: config.db.password,
    port: config.db.port,
    ssl:{
        rejectUnauthorized: false
    }
})

//function to return db pool
function returnDB() {
    return pool
}

module.exports = {
    returnDB
}