import UserContext from "./user_context";

import { useEffect, useReducer } from "react";

const defaultUserState = {
    user: {}
}

const userReducer = (state, action) => {
    if (action.id === 'ADD_USER') {
        localStorage.setItem('user', JSON.stringify(action.user));
        return {
            user: action.user
        }
    }
    if (action.id == 'REMOVE_USER') {
        localStorage.removeItem('user');
        return {
            user: {}
        }
    }
    return defaultUserState;
}


const UserProvider = ({ children }) => {
    useEffect(async () => {
        const localData = localStorage.getItem('user')
        if (localData) {
            const result = await JSON.parse(localData);
            addUserHandler(result);
        }
    }, []);
    const [userState, dispatchUserAction] = useReducer(userReducer, defaultUserState);

    const addUserHandler = (user) => {
        dispatchUserAction({ id: "ADD_USER", user: user });
    };

    const removeUserHandler = () => {
        dispatchUserAction({ id: "REMOVE_USER" });
    };

    const userContext = {
        user: userState.user,
        setUser: addUserHandler,
        removeUser: removeUserHandler
    }

    return (
        <UserContext.Provider value={userContext}>
            {children}
        </UserContext.Provider>
    )
};

export default UserProvider;