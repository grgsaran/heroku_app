const config = require("../../config");
const roleService = require("../services/roleService");
const authValidator = require("../validators/auth_validator");

module.exports = {
  parameters: [
    {
      name: 'api_key',
      in: 'header',
      type: 'string',
      description: 'token',
    },
  ],
  get: GET,
  post: POST,
}
async function GET(req, res, next) {
  const roles = await roleService.getRoles();
  if (Object.keys(roles).length != 0) {
    res.status(config.status_code.ok).send(roles);
  } else {
    res.status(config.status_code.no_content).send();
  }
}

async function POST(req, res, next) {
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    if (req.body.r_name.length > 0 && req.body.r_desc.length > 0 && req.body.r_permission.length > 0) {
      const query = await roleService.addRoles(req.body);
      console.log(query);
      if (query > 0) {
        res.status(config.status_code.created).send("New role created");
      } else {
        res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Role already exists" });
      }
    } else {
      res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Invalid data" });
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

GET.apiDoc = {
  summary: "Fetch roles.",
  operationId: "getRoles",
  tags: ['Roles'],
  responses: {
    200: {
      description: "List of roles.",
      schema: {
        type: "array",
        items: {
          $ref: "#/definitions/Role",
        },
      },
    },
  },
};

POST.apiDoc = {
  summary: "Add a new role.",
  operationId: "addRole",
  tags: ['Roles'],
  consumes: ["application/json"],
  parameters: [
    {
      in: "body",
      name: "role",
      schema: {
        $ref: "#/definitions/Role",
      },
    },
  ],
  responses: {
    201: {
      description: "New role created",
    },
    400: {
      description: 'Something happened...',
      schema: {
        $ref: '#/definitions/ErrorResponse'
      }
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
}