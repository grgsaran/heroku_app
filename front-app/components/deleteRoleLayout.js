import { useRouter } from "next/router";
import { useState } from "react";
import config from "../config";
import tokenWorker from "../store/token_worker";
import Card from "./card";

export default function DeleteRoleLayout(props) {
    const userCtx = props.context;

    const router = useRouter();

    const [deleteRoleName, setDeleteRoleName] = useState('')

    if (!tokenWorker.getTokenWorker()) {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (localUser) {
            const url = new URL('../refreshtoken.js', import.meta.url)
            let worker = new Worker(url);
            worker.onmessage = function (event) {
                if (event.data) {
                    userCtx.setUser(event.data);
                    tokenWorker.startTokenWorker(event.data);
                }
            }
            tokenWorker.setTokenWorker(worker);
            tokenWorker.startTokenWorker(localUser);
        }
    }

    window.onbeforeunload = (event) => {
        tokenWorker.terminateTokenWorker();
    }

    function deleteRoleBoxHandler(event){
        const inputRoleName = event.target.value.trim()
        setDeleteRoleName(inputRoleName)
        if (inputRoleName.length > 0) {
            if (!document.getElementById('deleteroleboxspan').hidden) {
                document.getElementById('deleteroleboxspan').hidden = true;
            }
        }
    }

    async function deleteRoleHandler(event){
        event.preventDefault()
        if(deleteRoleName.length>0){
            if(confirm(`Are you sure you want to delete role ${deleteRoleName}?`)){
                const res = await fetch(`${config.app.baseUrl}/roles/${deleteRoleName}`, {
                    method: 'DELETE',
                    body: JSON.stringify(),
                    headers: {
                        'Content-Type': 'application/json',
                        'api_key': userCtx.user.accessToken
                    }
                });
                if (res.status == '200') {
                    alert(`Role ${deleteRoleName} deleted successfully`)
                    router.replace('/')
                } else {
                    alert(`Error ${res.status} ${res.statusText}`)
                }
            }
        } else {
            document.getElementById('deleteroleboxspan').hidden = false;
        }
    }

    return (
        <Card className='justify-center md:w-3/5 place-self-center'>
            <div className='flex flex-col'>
                <div className='md:text-5xl text-white pt-2'>
                    Delete Role
                </div>
                <div id='deleteboxdiv' className='flex flex-col pt-2'>
                    <label htmlFor='deletebox' className='text-white'>
                        Enter Role Name:
                    </label>
                    <input name='deleterolebox' id='deleterolebox' onChange={deleteRoleBoxHandler} />
                    <span id='deleteroleboxspan' hidden={true} className='text-xs text-red-400'>
                        Enter Role Name to delete
                    </span>
                    <button id='deleterolebtn' onClick={deleteRoleHandler}
                        className='place-self-start mt-2 bg-blue-500 hover:bg-blue-700
                     text-white font-bold py-2 px-4 rounded'>Delete</button>
                </div>
            </div>
        </Card>
    )
}