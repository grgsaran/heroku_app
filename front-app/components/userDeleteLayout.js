import { useRouter } from "next/router";
import { useState } from "react";
import config from "../config";
import tokenWorker from "../store/token_worker";
import Card from "./card";

export default function UserDeleteLayout(props) {
    const userCtx = props.context;

    const router = useRouter();

    const [deleteId, setDeleteId] = useState('')

    if (!tokenWorker.getTokenWorker()) {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (localUser) {
            const url = new URL('../refreshtoken.js', import.meta.url)
            let worker = new Worker(url);
            worker.onmessage = function (event) {
                if (event.data) {
                    userCtx.setUser(event.data);
                    tokenWorker.startTokenWorker(event.data);
                }
            }
            tokenWorker.setTokenWorker(worker);
            tokenWorker.startTokenWorker(localUser);
        }
    }

    window.onbeforeunload = (event) => {
        tokenWorker.terminateTokenWorker();
    }

    function deleteIdBoxHandler(event) {
        const deleteitem = event.target.value
        setDeleteId(deleteitem.trim())
        if (deleteitem.trim().length > 0) {
            if (!document.getElementById('deleteboxspan').hidden) {
                document.getElementById('deleteboxspan').hidden = true;
            }
        }
    }

    async function deleteHandler(event) {
        event.preventDefault();
        console.log("Delete id ",deleteId);
        if(deleteId.length>0){
            if(confirm(`Are you sure you want to delete ${deleteId}?`)){
                const res = await fetch(`${config.app.baseUrl}/users/${deleteId}`, {
                    method: 'DELETE',
                    body: JSON.stringify(),
                    headers: {
                        'Content-Type': 'application/json',
                        'api_key': userCtx.user.accessToken
                    }
                });
                if (res.status == '200') {
                    alert(`${deleteId} deleted successfully`)
                    router.replace('/')
                } else {
                    alert(`Error ${res.status} ${res.statusText}`)
                }
            }
        } else {
            document.getElementById('deleteboxspan').hidden = false;   
        }
    }

    return (
        <Card className='justify-center md:w-3/5 place-self-center'>
            <div className='flex flex-col'>
                <div className='md:text-5xl text-white pt-2'>
                    Delete User
                </div>
                <div id='deleteboxdiv' className='flex flex-col pt-2'>
                    <label htmlFor='deletebox' className='text-white'>
                        Enter User ID:
                    </label>
                    <input name='deletebox' id='deletebox' onChange={deleteIdBoxHandler} />
                    <span id='deleteboxspan' hidden={true} className='text-xs text-red-400'>
                        Enter ID to delete
                    </span>
                    <button id='deletebtn' onClick={deleteHandler} className='place-self-start mt-2 bg-blue-500 hover:bg-blue-700
                     text-white font-bold py-2 px-4 rounded'>Delete</button>
                </div>
            </div>
        </Card>
    )
}