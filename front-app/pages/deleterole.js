import Head from 'next/head';
import { useContext, useEffect, useState } from 'react';
import DeleteRoleLayout from '../components/deleteRoleLayout';
import Layout from '../components/layout'
import UserContext from '../store/user_context';

export default function DeleteRole() {

    const userCtx = useContext(UserContext);

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (!localUser) {
            window.location.href = '/login'
        } else {
            const currentTime = new Date().getTime();
            const tokenExpiry = new Date(localUser.refreshTokenExpiry).getTime();
            const timeDifference = tokenExpiry - currentTime;
            if (timeDifference < 0) {
                window.localStorage.removeItem('user')
                window.location.href = '/login'
            } else {
                if (localUser.r_name != 'Admin') {
                    window.location.href = '/'
                } else {
                    setIsLoading(false);
                }
            }
        }
    });

    if (isLoading) {
        return (
            <div>Loading...</div>
        )
    }

    return (
        <Layout>
            <Head>
                <title>Delete Role</title>
            </Head>
            <DeleteRoleLayout context={userCtx} />
        </Layout>
    )
}