// query services
const dbCon = require('../../db_connection')
const crypto = require('crypto')

async function hashPassword(password) {
  const hash_password = await crypto.pbkdf2Sync(password, 'pit-m3d', 1000, 64, 'sha512').toString('hex');
  return hash_password;
}

const userService = {

  //returns all users
  async getUsers() {
    let result;
    await dbCon.returnDB().query('SELECT u_name, email, password, f_name, l_name, r_name FROM users, roles WHERE users.r_id = roles.r_id')
      .then((query) => {
        result = query;
        console.log(result.rows);
      }).catch((e) => {
        console.log(e);
      })
    if (result && result.rowCount) {
      return result.rows;
    } else {
      return {};
    }
  },

  //return user by user name
  async getUsersById(u_name) {
    let result;
    await dbCon.returnDB().query('SELECT u_name, email, password, f_name, l_name, r_name FROM users, roles WHERE u_name = $1 AND users.r_id = roles.r_id', [u_name])
      .then((query) => {
        result = query;
        console.log(result.rows);
      }).catch((e) => {
        console.log(e);
      })
    if (result && result.rowCount) {
      return result.rows;
    } else {
      return {};
    }
  },

  async loginUser(u_name, u_password) {
    let result;
    const hash_password = await hashPassword(u_password);
    console.log('Login Hash password: ', hash_password);
    await dbCon.returnDB()
      .query('SELECT u_name, email, password, f_name, l_name, r_name FROM users, roles WHERE u_name = $1 AND password = $2 AND users.r_id = roles.r_id', [u_name, hash_password])
      .then((query) => {
        result = query;
        console.log(result.rows);
      }).catch((e) => {
        console.log(e);
      })
    if (result && result.rowCount) {
      return result.rows;
    } else {
      return {};
    }
  },

  //add new user
  async addUser(req) {
    // console.log(req);
    const { u_name, email, password, f_name, l_name, r_name } = req;
    const hash_password = await hashPassword(password);
    console.log('Hash password: ', hash_password);
    let result;
    await dbCon.returnDB().query('INSERT INTO users (u_name, email, password, f_name, l_name, r_id) VALUES ($1, $2, $3, $4, $5, (SELECT r_id FROM roles WHERE r_name = $6))', [u_name, email, hash_password, f_name, l_name, r_name])
      .then((query) => {
        console.log(`Query count ${query.rowCount}`);
        result = query;
      }).catch((e) => {
        console.log(JSON.stringify(e));
      })
    if (result && result.rowCount) {
      return result.rowCount;
    } else {
      return 0;
    }
  },

  //update user by user name
  async updateUser(param_u_name, req) {
    const { u_name, email, password, f_name, l_name, r_name } = req
    let result;
    if (password.trim().length > 0) {
      const hash_password = await hashPassword(password);
      await dbCon.returnDB().query('UPDATE users set u_name = $1, email = $2, password = $3, f_name = $4, l_name = $5, r_id = (SELECT r_id FROM roles WHERE r_name = $6) WHERE u_name = $7', [u_name, email, hash_password, f_name, l_name, r_name, param_u_name])
        .then((query) => {
          console.log(`Query count ${query.rowCount}`);
          result = query;
        }).catch((e) => {
          console.log(e);
        })
      if (result && result.rowCount) {
        return result.rowCount;
      } else {
        return 0;
      }
    } else {
      await dbCon.returnDB().query('UPDATE users set u_name = $1, email = $2, f_name = $3, l_name = $4, r_id = (SELECT r_id FROM roles WHERE r_name = $5) WHERE u_name = $6', [u_name, email, f_name, l_name, r_name, param_u_name])
        .then((query) => {
          console.log(`Query count ${query.rowCount}`);
          result = query;
        }).catch((e) => {
          console.log(e);
        })
      if (result && result.rowCount) {
        return result.rowCount;
      } else {
        return 0;
      }
    }
  },

  //delete user by user name
  async deleteUser(u_name) {
    let result;
    await dbCon.returnDB().query('DELETE FROM users WHERE u_name = $1', [u_name])
      .then((query) => {
        console.log(`Query count ${query.rowCount}`);
        result = query;
      }).catch((e) => {
        console.log(e);
      })
    if (result && result.rowCount) {
      return result.rowCount;
    } else {
      return 0;
    }
  },

};

module.exports = userService;