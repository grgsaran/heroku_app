import tokenWorker from "../store/token_worker";
import Card from "./card";
import LogForm from "./logForm";

export default function AddLogLayout(props) {

    const userCtx = props.context

    if (!tokenWorker.getTokenWorker()) {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (localUser) {
            const url = new URL('../refreshtoken.js', import.meta.url)
            let worker = new Worker(url);
            worker.onmessage = function (event) {
                if (event.data) {
                    userCtx.setUser(event.data);
                    tokenWorker.startTokenWorker(event.data);
                }
            }
            tokenWorker.setTokenWorker(worker);
            tokenWorker.startTokenWorker(localUser);
        }
    }

    window.onbeforeunload = (event) => {
        tokenWorker.terminateTokenWorker();
    }

    return (
        <Card className='justify-center md:w-3/5 place-self-center'>
            <LogForm context={userCtx}/>
        </Card>
    )
}