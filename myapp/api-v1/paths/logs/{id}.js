const logService = require("../../services/logService");
const config = require("../../../config");
const authValidator = require("../../validators/auth_validator");

module.exports = {
  parameters: [
    {
      name: 'id',
      in: 'path',
      type: 'string',
      required: true,
      description: 'User name',
    },
    {
      name: 'api_key',
      in: 'header',
      type: 'string',
      description: 'token',
    },
  ],
  get: GET,
  delete: DELETE,
};

async function GET(req, res, next) {
  console.log(`Log for user ${req.params.id}`);
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    const roles = await logService.getLogsByUid(req.params.id);
    if (Object.keys(roles).length != 0) {
      res.status(config.status_code.ok).send(roles);
    } else {
      res.status(config.status_code.no_content).send();
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

async function DELETE(req, res, next) {
  console.log(`About to delete logs for user: ${req.params.id}`);
  const token = req.get("api_key");
  if (authValidator.authenticateToken(token, config.token_type.api_key)) {
    const query = await logService.deleteLogByUid(req.params.id);
    console.log(query);
    if (query > 0) {
      res.status(config.status_code.ok).send(`Logs deleted for user ${req.params.id}`);
    } else {
      res.status(config.status_code.bad_request).send({ "errorCode": config.status_code.bad_request, "message": "Logs not deleted." });
    }
  } else {
    res.status(config.status_code.unauthorized).send();
  }
}

GET.apiDoc = {
  summary: "Fetch log by user name.",
  operationId: "getLogByUid",
  tags: ['Logs'],
  responses: {
    200: {
      description: "Logs.",
      schema: {
        type: "object",
        items: {
          $ref: "#/definitions/Log",
        },
      },
    },
    204: {
      description: "Logs not found",
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
}

DELETE.apiDoc = {
  summary: "Delete log.",
  operationId: "deleteLog",
  tags: ['Logs'],
  consumes: ["application/json"],
  responses: {
    200: {
      description: "Delete",
    },
    400: {
      description: 'Something happened...',
      schema: {
        $ref: '#/definitions/ErrorResponse'
      }
    },
  },
  security: [
    {
      keyScheme: []
    }
  ]
}