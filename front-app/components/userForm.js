import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import config from "../config";
import validators from "../validators";

export default function UserForm(props) {

    const userCtx = props.context
    const roles = props.roles

    let userdata = {
        u_name: '',
        email: '',
        password: '',
        f_name: '',
        l_name: '',
        r_name: ''
    }
    if (props.userdata) {
        userdata = props.userdata
    }
    const mode = props.mode

    const router = useRouter();

    const [username, setUserName] = useState(userdata.u_name);
    const [email, setEmail] = useState(userdata.email);
    const [password, setPassword] = useState('');
    const [confirmpassword, setConfrimPassword] = useState('');
    const [fname, setFirstName] = useState(userdata.f_name);
    const [lname, setLastName] = useState(userdata.l_name);
    const [role, setRole] = useState(userdata.r_name);

    useEffect(() => {
        if (mode === 'edit' || mode === 'profile') {
            setUserName(userdata.u_name)
            setEmail(userdata.email)
            setFirstName(userdata.f_name)
            setLastName(userdata.l_name)
            setRole(userdata.r_name)
        }
    }, [userdata]);

    function userNameHandler(event) {
        setUserName(event.target.value.trim());
    }

    function emailHandler(event) {
        const enteredEmail = event.target.value;
        setEmail(enteredEmail.trim());
        if (enteredEmail && enteredEmail.trim().length > 0) {
            if (validators.emailValidator(event.target.value.trim())) {
                document.getElementById('emailspan').hidden = true;
            } else {
                document.getElementById('emailspan').hidden = false;
            }
        } else {
            document.getElementById('emailspan').hidden = false;
        }
    }

    function passwordHandler(event) {
        const enteredPassword = event.target.value;
        setPassword(enteredPassword.trim());
        if (enteredPassword && enteredPassword.trim().length > 0) {
            if (validators.passwordValidator(enteredPassword)) {
                document.getElementById('passwordspan').hidden = true;
            } else {
                document.getElementById('passwordspan').hidden = false;
            }
        } else {
            document.getElementById('passwordspan').hidden = false;
        }
    }

    function confirmPasswordHandler(event) {
        const enteredConfrimPassword = event.target.value;
        setConfrimPassword(enteredConfrimPassword.trim());
        if (enteredConfrimPassword && enteredConfrimPassword.trim().length > 0) {
            if (password === enteredConfrimPassword.trim()) {
                document.getElementById('confirmpasswordspan').hidden = true;
            } else {
                document.getElementById('confirmpasswordspan').hidden = false;
            }
        } else {
            document.getElementById('confirmpasswordspan').hidden = false;
        }
    }

    function firstNameHandler(event) {
        setFirstName(event.target.value.trim());
    }

    function lastNameHandler(event) {
        setLastName(event.target.value.trim());
    }

    function roleHandler(event) {
        setRole(event.target.value.trim());
    }

    async function onSubmit(event) {
        event.preventDefault();
        if (mode === 'edit' || mode === 'profile') {
            if (username.length > 0 && email.length > 0 &&
                fname.length > 0 && lname.length > 0 && role.length > 0) {
                if (validators.emailValidator(email)) {
                    if (password.length > 0 || confirmpassword.length > 0) {
                        if (!validators.passwordValidator(password)) {
                            console.log("flase valid");
                            alert('Password requirements not matched');
                            return;
                        }
                        if (password != confirmpassword) {
                            alert('Passwords do not match')
                            return;
                        }
                    }
                    const res = await fetch(`${config.app.baseUrl}/users/${userdata.u_name}`, {
                        method: 'PUT',
                        body: JSON.stringify({
                            u_name: username,
                            email: email,
                            password: password,
                            f_name: fname,
                            l_name: lname,
                            r_name: role
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                            'api_key': userCtx.user.accessToken
                        }
                    });
                    if (res.status == '200') {
                        alert("User updated successfully")
                        if (mode === 'profile') {
                            userCtx.removeUser();
                            router.replace('/');
                        } else {
                            router.replace('/')
                        }
                    } else {
                        alert(`Error ${res.status}`)
                    }
                } else {
                    alert('Invalid email');
                }
            } else {
                alert("Enter all fields");
            }
        } else {
            if (username.length > 0 && email.length > 0 && password.length > 0 &&
                confirmpassword.length > 0 && fname.length > 0 && lname.length > 0) {
                if (validators.emailValidator(email)) {
                    if (validators.passwordValidator(password)) {
                        if (password === confirmpassword) {
                            const res = await fetch(`${config.app.baseUrl}/users`, {
                                method: 'POST',
                                body: JSON.stringify({
                                    u_name: username,
                                    email: email,
                                    password: password,
                                    f_name: fname,
                                    l_name: lname,
                                    r_name: role
                                }),
                                headers: {
                                    'Content-Type': 'application/json',
                                    'api_key': userCtx.user.accessToken
                                }
                            });
                            if (res.status == '201') {
                                alert("User added successfully")
                                router.replace('/');
                            } else {
                                alert(`Error ${res.status}`)
                            }
                        } else {
                            alert('Passwords do not match')
                        }
                    } else {
                        alert('Password requirements not matched');
                    }
                } else {
                    alert('Invalid email');
                }
            } else {
                alert("Enter all field");
            }
        }
    }

    return (
        <div id={props.id} style={{ display: props.display }} className='flex flex-col'>
            <div className='md:text-5xl text-white pt-3'>
                Enter User Details
            </div>
            {mode != 'edit' || mode != 'profile' && <span className='text-xs text-red-400'>
                All fields are mandatory
            </span>}
            {mode === 'edit' || mode === 'profile' && <span className='text-xs text-red-400'>
                Leave password blank to keep password unchanged
            </span>}
            <div className='flex flex-col pt-3'>
                <label htmlFor='username' className='text-white'>Username</label>
                <input name='username' id='username' defaultValue={userdata.u_name} onChange={userNameHandler}
                    className='bg-white mt-1' type='text' />
            </div>
            <div className='flex flex-col pt-3'>
                <label htmlFor='email' className='text-white'>Email</label>
                <input name='email' id='email' defaultValue={userdata.email} onChange={emailHandler}
                    className='bg-white mt-1' />
                <span id='emailspan' hidden={true} className='text-xs text-red-400'>
                    Enter valid email
                </span>
            </div>
            <div className='flex flex-col pt-3'>
                <label htmlFor='password' className='text-white'>Password</label>
                <input name='password' onChange={passwordHandler} id='password'
                    className='bg-white mt-1' type='password' />
                <span id='passwordspan' hidden={true} className='text-xs text-red-400'>
                    Password must be combination of at least <br />
                    1 lowercase letter,
                    1 uppercase letter, 1 number, <br />1 special character and
                    at least 8 characters long
                </span>
            </div>
            <div className='flex flex-col pt-3'>
                <label htmlFor='confirmpassword' className='text-white'>
                    Confirm Password
                </label>
                <input name='confirmpassword' onChange={confirmPasswordHandler}
                    id='confirmpassword' className='bg-white mt-1' type='password' />
                <span id='confirmpasswordspan' hidden={true} className='text-xs
                     text-red-400'>
                    Passwords does not match
                </span>
            </div>
            <div className='flex flex-col pt-3'>
                <label htmlFor='fname' className='text-white'>First Name</label>
                <input name='fname' id='fname' defaultValue={userdata.f_name} onChange={firstNameHandler}
                    className='bg-white mt-1' />
            </div>
            <div className='flex flex-col pt-3'>
                <label htmlFor='lname' className='text-white'>Last Name</label>
                <input name='lname' id='lname' defaultValue={userdata.l_name} onChange={lastNameHandler}
                    className='bg-white mt-1' />
            </div>
            {mode != 'profile' &&
                <div className='flex flex-col pt-3'>
                    <label htmlFor='roles' className='text-white'>Enter Role:</label>
                    <div className='flex text-xs text-red-400'>
                        {roles.map((role, i) => (
                            <span id={role.r_name} key={role.r_name}>
                                {i + 1}.{role.r_name}&nbsp;&nbsp;
                            </span>
                        ))}
                    </div>
                    <input name='roles' id='roles' defaultValue={userdata.r_name} placeholder='Admin' onChange={roleHandler}
                        className='bg-white mt-1' />
                </div>}
            <div className='flex pt-8 pb-3 justify-center'>
                <button id='submit' onClick={onSubmit} className='bg-blue-500 hover:bg-blue-700
                     text-white font-bold py-2 px-4 rounded'>
                    Submit
                </button>
                {mode === 'edit' || mode === 'profile' && <button id='cancel' onClick={props.onCancel}
                    className='bg-blue-500 hover:bg-blue-700 text-white 
                    font-bold py-2 px-4 rounded ml-2'>
                    Cancel
                </button>}
            </div>
        </div>
    )
}