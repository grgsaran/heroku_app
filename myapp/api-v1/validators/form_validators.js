const formValidators = {

    emailValidator(email) {
        let emailRegex = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
        return emailRegex.test(email);
    },

    isValidData(req) {
        if (req.body.u_name.length > 0 && req.body.f_name.length > 0
            && req.body.l_name.length > 0 && req.body.email.length > 0
            && req.body.password.length > 0 && req.body.r_name.length > 0) {
            return true;
        } else {
            return false;
        }
    },
    isValidDataNoPassword(req) {
        if (req.body.u_name.length > 0 && req.body.f_name.length > 0
            && req.body.l_name.length > 0 && req.body.email.length > 0
            && req.body.r_name.length > 0) {
            return true;
        } else {
            return false;
        }
    }
};

module.exports = formValidators;