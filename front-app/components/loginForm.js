import Card from "./card";
import { useContext, useState } from 'react';
import { useRouter } from 'next/router'
import UserContext from '../store/user_context';
import config from "../config";

export default function LoginForm() {

    const userCtx = useContext(UserContext);

    const router = useRouter();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    async function onLogin(event) {
        event.preventDefault();
        if (username.trim().length === 0 && password.trim().length === 0) {
            alert("Please enter username and password")
            return;
        }
        const res = await fetch(`${config.app.baseUrl}/auth`, {
            method: 'POST',
            body: JSON.stringify({
                user_name: username,
                password: password
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        console.log("Response ", res.status);
        if (res.status == '200') {
            const data = await res.json();
            console.log(data[0]);
            setUsername('');
            setPassword('');
            userCtx.setUser(data[0]);
            router.replace('/');
        } else {
            console.log("User not found");
            alert("User not found");
        }
    }

    function usernameHandler(event) {
        setUsername(event.target.value);
    }

    function passwordHandler(event) {
        setPassword(event.target.value);
    }

    return (
        <Card>
            <div className="flex flex-wrap flex-col place-content-start p-7">
                <div>
                    <span className="font-mono text-white text-base md:font-semibold">
                        PIT-M3D User Management System</span>
                </div>
                <div >
                    <form className="p-7">
                        <div className="flex flex-wrap flex-col">
                            <label htmlFor="username" className="block text-white 
                text-sm font-bold md:mb-2">Username</label>
                            <input id="username" name="username"
                                onChange={usernameHandler}
                                value={username}
                                className="border-2 md:mb-2" type="text" required
                                placeholder="Username" />
                        </div>
                        <div className="flex flex-wrap flex-col">
                            <label htmlFor="password" className="block text-white 
              text-sm font-bold md:mb-2">Password</label>
                            <input id="password" name="password"
                                value={password}
                                className="border-2 md:mb-2"
                                onChange={passwordHandler} type="password" required placeholder="Password" />
                        </div>
                        <button className="bg-blue-500 hover:bg-blue-700 text-white 
              font-bold py-2 px-4 rounded"  onClick={onLogin} type="button">
                            Login
                        </button>

                    </form>
                </div>
            </div>
        </Card>
    )
}