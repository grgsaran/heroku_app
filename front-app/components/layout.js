import { Fragment } from "react";
import NavigationBar from "./navigation_bar";

export default function Layout({ children }) {
  const classes = 'flex flex-wrap flex-col bg-white w-screen h-screen md:mx-1' + children.className;
  return (
    <Fragment>
      <NavigationBar />
      <div className={classes}>{children}</div>
    </Fragment>
  )
}
