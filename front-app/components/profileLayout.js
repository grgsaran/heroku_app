import { useRouter } from "next/router";
import tokenWorker from "../store/token_worker";
import Card from "./card";
import UserForm from "./userForm";

export default function ProfileLayout(props) {

    const userCtx = props.context;
    const router = useRouter();

    if (!tokenWorker.getTokenWorker()) {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (localUser) {
            const url = new URL('../refreshtoken.js', import.meta.url)
            let worker = new Worker(url);
            worker.onmessage = function (event) {
                if (event.data) {
                    userCtx.setUser(event.data);
                    tokenWorker.startTokenWorker(event.data);
                }
            }
            tokenWorker.setTokenWorker(worker);
            tokenWorker.startTokenWorker(localUser);
        }
    }

    window.onbeforeunload = (event) => {
        tokenWorker.terminateTokenWorker();
    }

    const user = JSON.parse(window.localStorage.getItem('user'));
    if (user) {
        delete user.accessToken;
        delete user.accessTokenExpiry;
        delete user.loginDate;
        delete user.refreshToken;
        delete user.refreshTokenExpiry;
    }

    function cancelHandler(event) {
        event.preventDefault();
        router.push('/');
    }

    return (
        <Card className='justify-center md:w-3/5 place-self-center'>
            <UserForm id='userform' mode='profile' onCancel={cancelHandler}
                userdata={user} display='block' context={userCtx} />
        </Card>
    )
}