import Layout from '../components/layout'
import Head from 'next/head'
import UserContext from '../store/user_context';
import { useContext, useEffect, useState } from 'react';
import ProfileLayout from '../components/profileLayout';

export default function Profile(props) {

    const usrCtx = useContext(UserContext);
    console.log("User context is ", usrCtx);

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const localUser = JSON.parse(window.localStorage.getItem('user'))
        if (!localUser) {
            window.location.href = '/login'
        } else {
            const currentTime = new Date().getTime();
            const tokenExpiry = new Date(localUser.refreshTokenExpiry).getTime();
            const timeDifference = tokenExpiry - currentTime;
            if (timeDifference < 0) {
                window.localStorage.removeItem('user')
                window.location.href = '/login'
            } else {
                setIsLoading(false);
            }
        }
    });

    if (isLoading) {
        return (
            <div>Loading...</div>
        )
    }

    return (
        <Layout>
            <Head>
                <title>Profile</title>
            </Head>
            <ProfileLayout context={usrCtx} />
        </Layout>
    )
}