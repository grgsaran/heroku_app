//main api
const express = require('express')
const app = express()
const config = require('./config')
const http = require('http')
const { initialize } = require('express-openapi')
const swaggerUi = require('swagger-ui-express');
const cors = require('cors')

//schema validation
let OpenAPISchemaValidator = require('openapi-schema-validator').default;
const apiDoc = require('./api-v1/api-doc')

let validator = new OpenAPISchemaValidator({
  version: 2,
  // optional
  extensions: {
    /* place any properties here to extend the schema. */
  }
});
console.log(validator.validate(apiDoc));

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors());

const PORT = process.env.PORT || config.app.port;

http.createServer(app).listen(PORT, () => {
  console.log(`My app listening at https://damp-sierra-29447.herokuapp.com`)
})

// OpenAPI routes
initialize({
  app,
  apiDoc: require("./api-v1/api-doc"),
  dependencies: {
    userService: require("./api-v1/services/userService"),
    roleService: require("./api-v1/services/roleService"),
    logService: require("./api-v1/services/logService"),
  },
  paths: "./api-v1/paths",
});

//OpenAPI UI
app.use(
  "/api-documentation",
  swaggerUi.serve,
  swaggerUi.setup(null, {
    swaggerOptions: {
      url: `https://damp-sierra-29447.herokuapp.com/api-docs`,
    },
  })
);

module.exports = app;