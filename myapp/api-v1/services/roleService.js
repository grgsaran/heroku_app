//role query services
const dbCon = require('../../db_connection')

const roleService = {
    
    async getRoles(){
        let result;
        await dbCon.returnDB().query('SELECT r_name, r_desc, r_permission FROM roles')
        .then((query) => {
            result = query;
            console.log(result.rows);
        }).catch((e)=>{
            console.log(e);
        })
        if(result && result.rowCount){
            return result.rows;
        } else {
            return {};
        }
    },

    async addRoles(req){
        const {r_name, r_desc, r_permission} = req;
        let result;
        await dbCon.returnDB().query('INSERT into roles (r_name, r_desc, r_permission) VALUES ($1, $2, $3)', [r_name, r_desc, r_permission])
        .then((query) => {
            console.log(`Query count ${query.rowCount}`);
            result = query;
        }).catch((e) => {
            console.log(e);
        })
        if (result && result.rowCount){
            return result.rowCount;
        } else {
            return 0;
        }
    },

    async getRoleById(r_name){
        let result;
        await dbCon.returnDB().query('SELECT r_name, r_desc, r_permission FROM roles WHERE r_name = $1', [r_name])
        .then((query) => {
            result = query;
            console.log(result.rows);
        }).catch((e) => {
            console.log(e);
        })
        if(result && result.rowCount){
            return result.rows;
        } else {
            return {};
        }
    },

    async updateRole(param_r_name, req){
        const {r_name, r_desc, r_permission} = req
        let result;
        await dbCon.returnDB().query('UPDATE roles set r_name = $1, r_desc = $2, r_permission = $3 WHERE r_name = $4', [r_name, r_desc, r_permission, param_r_name])
        .then((query) => {
            console.log(`Query count ${query.rowCount}`);
            result = query;
        }).catch((e) => {
            console.log(e);
        });
        if (result && result.rowCount){
            return result.rowCount;
        } else {
            return 0;
        }
    },

    async deleteRole(r_name){
        let result;
        await dbCon.returnDB().query('DELETE from roles where r_name = $1', [r_name])
        .then((query) => {
            console.log(`Query count ${query.rowCount}`);
            result = query;
        }).catch((e) => {
            console.log(e);
        });
        if (result && result.rowCount){
            return result.rowCount;
        } else {
            return 0;
        }
    }

}

module.exports = roleService;